# Import required modules
import glob
import torch
from PIL import Image
import csv
import os
import time

# Open the file
f = open('data.csv','w',newline='')
writer = csv.writer(f)

# First line of the file2
header = ['Image number','Width','Height','Time taken','Number of objects found','Accuracy']
writer.writerow(header)

# Load the model
model = torch.hub.load('ultralytics/yolov5', 'yolov5s')

img_num = 0

# Get the current working directory to find images
cwd = os.getcwd()
for filename in glob.glob(cwd +r'/images/*.jpg'):
    line = []
    try:
        # open the images file and collect the relevant data to write to the file 
        im=Image.open(filename)
        imgs = [im]
        img_num += 1
        width, height = im.size
        line.append(('new' +(filename.split("/")[-1])))
        line.append(img_num)
        line.append(width)
        line.append(height)

        # Times the object detection
        start = time.time()
        results = model(imgs)  
        end = time.time()
        line.append((end-start)*1000)

        # Count the number of objects
        line.append(len(results.pandas().xyxy[0]))

        # Display results
        results.print()  

        # Calculate and write average accuracy to file
        avgaccuracy = 0
        for x in range(len(results.pandas().xyxy[0])):
            avgaccuracy += (results.pandas().xyxy[0].at[0,'confidence'])
        avgaccuracy /= len(results.pandas().xyxy[0])
        line.append(avgaccuracy)
        writer.writerow(line)
    except:
        print('Error')

# Close the file
f.close()