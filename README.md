# Final Year Project - Aqsa Isra Saied

## Description
This repository supports my final year project. In this repository you will find all code and results used for this project.

## Installation
This project has been developed and tested on Windows 10 and should be run on Windows 10.
All code in this project uses Python3. To install all required libraries please enter the following lines into terminal.

```console
git clone https://gitlab.com/sc18ais/final-year-project.git
cd final-year-project/Python
pip3 install -r requirements.txt
```

To download images code has been provided. This code requires the python module selenium. To install selenium enter:
```console
pip install selenium
```

Selenium uses firefox to work so it needs firefox installed and geckodriver: https://github.com/mozilla/geckodriver/releases. Download and extract geckodriver.exe file in the Python folder. 
If you want to use another browser such as Chrome, check selenium docs: https://selenium-python.readthedocs.io/installation.html

## Usage
To demonstrate this code images will have to be sourced and stored in the file Python/images. This can be done manually or via the code in Python/Getimages.py.
To use this code please enter the following commands.
```console
cd final-year-project/Python
python3 GetImages.py
```

Once images have been sourced the main.py code can be run to the speed and accuracy of the object detection model on your device.
```console
cd final-year-project/Python
python3 main.py
```

## Support
For help using the Getimages.py code please see https://github.com/panbak/pinterest-board-image-downloader 
For help using YOLOv5 please see https://github.com/ultralytics/yolov5 
For any other assistance please email sc18ais@leeds.ac.uk.

## Authors and acknowledgment
Thank you to Dr Zheng Wang for all your help and guidance with this project!
Thank you to Glenn Jocher and Panagiotis Bakas, the authors of the repositories listed above. 
